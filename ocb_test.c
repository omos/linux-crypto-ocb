#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/completion.h>
#include <linux/module.h>

#include <crypto/internal/aead.h>

#include "ocb_tv.h"

struct result {
	struct completion comp;
	int err;
};

static void result_complete(struct crypto_async_request *req, int err)
{
	struct result *res = req->data;

	if (err == -EINPROGRESS)
		return;

	res->err = err;
	complete(&res->comp);
}

static int aead_do_sync(int (*func)(struct aead_request *req),
			struct aead_request *req)
{
	struct result res;
	int err;

	res.err = 0;
	init_completion(&res.comp);

	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
				  &result_complete, &res);
	err = func(req);
	switch (err) {
	case 0:
		break;
	case -EINPROGRESS:
	case -EBUSY:
		wait_for_completion(&res.comp);
		err = res.err;
		if (err == 0)
			break;
		/* fall through */
	default:
		break;
	}
	return err;
}

static void dump_mismatch(const u8 *actual, const u8 *expected, unsigned length)
{
	unsigned i;

	printk("  actual:   ");
	for (i = 0; i < length; i++)
		pr_cont("%02x", (unsigned)actual[i]);
	pr_cont("\n");

	printk("  expected: ");
	for (i = 0; i < length; i++)
		pr_cont("%02x", (unsigned)expected[i]);
	pr_cont("\n");
}

static int run_test_case_inplace(const struct crypto_ocb_test_case *vec,
				 unsigned int number)
{
	int err = 0;
	int failed = 0;
	struct crypto_aead *cipher = NULL;
	struct aead_request *req = NULL;
	u8 *buffer = NULL, *buffer_ad, *buffer_crypt, *buffer_tag;
	u8 *iv = NULL;
	struct scatterlist sg[1];
	unsigned int buffer_len = vec->assoc_data_len + vec->plaintext_len + vec->tag_len;

	buffer = kmalloc(max(1U, buffer_len), GFP_KERNEL);
	if (!buffer) {
		printk("ocb_test: ERROR allocating buffer!\n");
		goto out;
	}
	buffer_ad = buffer;
	buffer_crypt = buffer_ad + vec->assoc_data_len;
	buffer_tag = buffer_crypt + vec->plaintext_len;

	sg_init_one(sg, buffer, buffer_len);

	cipher = crypto_alloc_aead("ocb(aes)", 0, 0);
	if (IS_ERR(cipher)) {
		printk("ocb_test: ERROR allocating cipher!\n");
		err = PTR_ERR(cipher);
		cipher = NULL;
		goto out;
	}

	iv = kmalloc(crypto_aead_ivsize(cipher), GFP_KERNEL);
	if (!iv) {
		printk("ocb_test: ERROR allocating IV!\n");
		goto out;
	}

	err = crypto_aead_setauthsize(cipher, vec->tag_len);
	if (err) {
		printk("ocb_test: ERROR setting authsize!\n");
		goto out;
	}

	err = crypto_aead_setkey(cipher, vec->key, vec->key_len);
	if (err) {
		printk("ocb_test: ERROR setting key!\n");
		goto out;
	}

	req = aead_request_alloc(cipher, GFP_KERNEL);
	if (IS_ERR(req)) {
		printk("ocb_test: ERROR allocating request!\n");
		err = PTR_ERR(req);
		req = NULL;
		goto out;
	}

	memcpy(iv, vec->nonce, crypto_aead_ivsize(cipher));
	memcpy(buffer_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buffer_crypt, vec->plaintext, vec->plaintext_len);

	aead_request_set_tfm(req, cipher);
	aead_request_set_ad(req, vec->assoc_data_len);
	aead_request_set_crypt(req, sg, sg, vec->plaintext_len, iv);

	err = aead_do_sync(crypto_aead_encrypt, req);
	if (err) {
		printk("ocb_test: ERROR encrypting!\n");
		goto out;
	}

	if (crypto_memneq(buffer_crypt, vec->ciphertext, vec->plaintext_len)) {
		failed += 1;
		printk("ocb_test: %u-ip-encryption-ciphertext: Testcase failed!\n", number);
		dump_mismatch(buffer_crypt, vec->ciphertext, vec->plaintext_len);
	}

	if (crypto_memneq(buffer_tag, vec->tag, vec->tag_len)) {
		failed += 1;
		printk("ocb_test: %u-ip-encryption-tag: Testcase failed!\n", number);
		dump_mismatch(buffer_tag, vec->tag, vec->tag_len);
	}

	if (crypto_memneq(buffer_ad, vec->assoc_data, vec->assoc_data_len)) {
		failed += 1;
		printk("ocb_test: %u-ip-encryption-ad: Testcase failed!\n", number);
		dump_mismatch(buffer_ad, vec->assoc_data, vec->assoc_data_len);
	}

	memcpy(iv, vec->nonce, crypto_aead_ivsize(cipher));
	memcpy(buffer_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buffer_crypt, vec->ciphertext, vec->plaintext_len);
	memcpy(buffer_tag, vec->tag, vec->tag_len);

	aead_request_set_ad(req, vec->assoc_data_len);
	aead_request_set_crypt(req, sg, sg, vec->plaintext_len + vec->tag_len,
			       iv);

	err = aead_do_sync(crypto_aead_decrypt, req);
	if (err == -EBADMSG) {
		err = 0;
		failed += 1;
		printk("ocb_test: %u-ip-decryption-tag: Testcase failed!\n", number);
	} else if (err) {
		printk("ocb_test: ERROR decrypting!\n");
		goto out;
	}

	if (crypto_memneq(buffer_crypt, vec->plaintext, vec->plaintext_len)) {
		failed += 1;
		printk("ocb_test: %u-ip-decryption-plaintext: Testcase failed!\n", number);
		dump_mismatch(buffer_crypt, vec->plaintext, vec->plaintext_len);
	}

	if (crypto_memneq(buffer_ad, vec->assoc_data, vec->assoc_data_len)) {
		failed += 1;
		printk("ocb_test: %u-ip-decryption-ad: Testcase failed!\n", number);
		dump_mismatch(buffer_ad, vec->assoc_data, vec->assoc_data_len);
	}

out:
	if (buffer)
		kfree(buffer);
	if (cipher)
		crypto_free_aead(cipher);
	if (iv)
		kfree(iv);
	if (req)
		aead_request_free(req);
	return err < 0 ? err : failed;
}

static int run_test_case(const struct crypto_ocb_test_case *vec,
			 unsigned int number)
{
	int err = 0;
	int failed = 0;
	struct crypto_aead *cipher = NULL;
	struct aead_request *req = NULL;
	u8 *buf_in = NULL, *buf_in_ad, *buf_in_crypt;
	u8 *buf_out = NULL, *buf_out_ad, *buf_out_crypt, *buf_out_tag;
	u8 *iv = NULL;
	struct scatterlist sg_in[1], sg_out[1];
	unsigned int buf_in_len = vec->assoc_data_len + vec->plaintext_len;
	unsigned int buf_out_len = vec->assoc_data_len + vec->plaintext_len
			+ vec->tag_len;

	buf_in = kmalloc(max(1U, buf_in_len), GFP_KERNEL);
	if (!buf_in) {
		printk("ocb_test: ERROR allocating input buffer!\n");
		goto out;
	}
	buf_in_ad = buf_in;
	buf_in_crypt = buf_in_ad + vec->assoc_data_len;

	sg_init_one(sg_in, buf_in, buf_in_len);

	buf_out = kmalloc(max(1U, buf_out_len), GFP_KERNEL);
	if (!buf_out) {
		printk("ocb_test: ERROR allocating output buffer!\n");
		goto out;
	}
	buf_out_ad = buf_out;
	buf_out_crypt = buf_out_ad + vec->assoc_data_len;
	buf_out_tag = buf_out_crypt + vec->plaintext_len;

	sg_init_one(sg_out, buf_out, buf_out_len);

	cipher = crypto_alloc_aead("ocb(aes)", 0, 0);
	if (IS_ERR(cipher)) {
		printk("ocb_test: ERROR allocating cipher!\n");
		err = PTR_ERR(cipher);
		cipher = NULL;
		goto out;
	}

	iv = kmalloc(crypto_aead_ivsize(cipher), GFP_KERNEL);
	if (!iv) {
		printk("ocb_test: ERROR allocating IV!\n");
		goto out;
	}

	err = crypto_aead_setauthsize(cipher, vec->tag_len);
	if (err) {
		printk("ocb_test: ERROR setting authsize!\n");
		goto out;
	}

	err = crypto_aead_setkey(cipher, vec->key, vec->key_len);
	if (err) {
		printk("ocb_test: ERROR setting key!\n");
		goto out;
	}

	req = aead_request_alloc(cipher, GFP_KERNEL);
	if (IS_ERR(req)) {
		printk("ocb_test: ERROR allocating request!\n");
		err = PTR_ERR(req);
		req = NULL;
		goto out;
	}

	memcpy(iv, vec->nonce, crypto_aead_ivsize(cipher));
	memcpy(buf_in_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buf_out_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buf_in_crypt, vec->plaintext, vec->plaintext_len);

	aead_request_set_tfm(req, cipher);
	aead_request_set_ad(req, vec->assoc_data_len);
	aead_request_set_crypt(req, sg_in, sg_out, vec->plaintext_len, iv);

	err = aead_do_sync(crypto_aead_encrypt, req);
	if (err) {
		printk("ocb_test: ERROR encrypting!\n");
		goto out;
	}

	if (crypto_memneq(buf_out_crypt, vec->ciphertext, vec->plaintext_len)) {
		failed += 1;
		printk("ocb_test: %u-encryption-ciphertext: Testcase failed!\n", number);
		dump_mismatch(buf_out_crypt, vec->ciphertext, vec->plaintext_len);
	}

	if (crypto_memneq(buf_out_tag, vec->tag, vec->tag_len)) {
		failed += 1;
		printk("ocb_test: %u-encryption-tag: Testcase failed!\n", number);
		dump_mismatch(buf_out_tag, vec->tag, vec->tag_len);
	}

	if (crypto_memneq(buf_out_ad, vec->assoc_data, vec->assoc_data_len)) {
		failed += 1;
		printk("ocb_test: %u-encryption-ad: Testcase failed!\n", number);
		dump_mismatch(buf_out_ad, vec->assoc_data, vec->assoc_data_len);
	}

	memcpy(iv, vec->nonce, crypto_aead_ivsize(cipher));
	memcpy(buf_out_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buf_in_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buf_out_crypt, vec->ciphertext, vec->plaintext_len);
	memcpy(buf_out_tag, vec->tag, vec->tag_len);

	aead_request_set_ad(req, vec->assoc_data_len);
	aead_request_set_crypt(req, sg_out, sg_in, vec->plaintext_len +
			       vec->tag_len, iv);

	err = aead_do_sync(crypto_aead_decrypt, req);
	if (err == -EBADMSG) {
		err = 0;
		failed += 1;
		printk("ocb_test: %u-decryption-tag: Testcase failed!\n", number);
	} else if (err) {
		printk("ocb_test: ERROR decrypting!\n");
		goto out;
	}

	if (crypto_memneq(buf_in_crypt, vec->plaintext, vec->plaintext_len)) {
		failed += 1;
		printk("ocb_test: %u-decryption-plaintext: Testcase failed!\n", number);
		dump_mismatch(buf_in_crypt, vec->plaintext, vec->plaintext_len);
	}

	if (crypto_memneq(buf_in_ad, vec->assoc_data, vec->assoc_data_len)) {
		failed += 1;
		printk("ocb_test: %u-decryption-ad: Testcase failed!\n", number);
		dump_mismatch(buf_in_ad, vec->assoc_data, vec->assoc_data_len);
	}

out:
	if (buf_in)
		kfree(buf_in);
	if (buf_out)
		kfree(buf_out);
	if (cipher)
		crypto_free_aead(cipher);
	if (iv)
		kfree(iv);
	if (req)
		aead_request_free(req);
	return err < 0 ? err : failed;
}

static int crypto_ocb_run_tests(void)
{
	unsigned int i = 0, ncases = ARRAY_SIZE(crypto_ocb_test_cases);
	int res, failed = 0;

	printk("ocb_test: Running tests...\n");
	for (i = 0; i < ncases; i++) {
		printk("ocb_test: Running testcase %u...\n", i);

		res = run_test_case(&crypto_ocb_test_cases[i], i);
		if (res < 0) {
			return -EINVAL;
		}
		failed += res;

		printk("ocb_test: Running testcase %u (in-place)...\n", i);
		res = run_test_case_inplace(&crypto_ocb_test_cases[i], i);
		if (res < 0) {
			return -EINVAL;
		}
		failed += res;
	}
	if (failed) {
		printk("ocb_test: FAIL: %i tests failed!\n", failed);
	} else {
		printk("ocb_test: OK!\n");
	}
	return 0;
}

static int __init crypto_ocb_test_module_init(void)
{
	int err = crypto_ocb_run_tests();
	if (err) {
		printk("ocb_test: ERROR: %i\n", err);
	}
	return 0;
}

static void __exit crypto_ocb_test_module_exit(void)
{
}

module_init(crypto_ocb_test_module_init);
module_exit(crypto_ocb_test_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ondrej Mosnacek <omosnacek@gmail.com>");
MODULE_DESCRIPTION("OCB AEAD mode -- tests");
MODULE_ALIAS_CRYPTO("ocb_test");
