/*
 * The OCB Authenticated-Encryption Algorithm
 * RFC 7253: https://tools.ietf.org/html/rfc7253
 *
 * Copyright (c) 2016 Ondrej Mosnacek <omosnacek@gmail.com>
 *
 * Based on gcm.c
 * Copyright (c) 2007 Nokia Siemens Networks - Mikko Herranen <mh1@iki.fi>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */

#include <crypto/algapi.h>
#include <crypto/b128ops.h>
#include <crypto/internal/aead.h>
#include <crypto/internal/skcipher.h>
#include <crypto/scatterwalk.h>
#include <linux/count_zeros.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/scatterlist.h>

#define OCB_BLOCK_SIZE 16
#define OCB_BLOCK_MASK (-OCB_BLOCK_SIZE)
#define OCB_NONCE_SIZE 12 /* FIXME */

union ocb_block {
	be128 b128;
	u8 bytes[OCB_BLOCK_SIZE];
};

struct ocb_instance_ctx {
	struct crypto_spawn spawn;
	struct crypto_skcipher_spawn ecb_spawn;
};

struct ocb_ctx {
	struct crypto_cipher *child;
	struct crypto_skcipher *ecb;
	union ocb_block l_star, l_dollar;
	union ocb_block l[sizeof(unsigned int) * 8]; /* log2(max payload size) */
};

struct ocb_req_ctx {
	struct aead_request *parent;

	int (*crypt_fn)(struct skcipher_request *req);
	int (*req_done)(struct ocb_req_ctx *rctx, u32 flags);

	struct scatterlist sg_src[2];
	struct scatterlist sg_dst[2];

	struct scatterlist *src;
	struct scatterlist *dst;

	int decrypting;
	unsigned int authsize;
	unsigned int cryptlen;

	union ocb_block tag, iv_offset, checksum;

	struct skcipher_request ecb_req CRYPTO_MINALIGN_ATTR;
};

static inline void ocb_block_set_zero(union ocb_block *out)
{
	out->b128.a = 0U;
	out->b128.b = 0U;
}

static inline int ocb_block_is_zero(union ocb_block *out)
{
	return (out->b128.a | out->b128.b) == 0U;
}

static inline void ocb_block_xor(union ocb_block *res, const union ocb_block *x,
				 const union ocb_block *y)
{
	be128_xor(&res->b128, &x->b128, &y->b128);
}

/* this is an inlinable version of gf128mul_x_bbe() */
/* it causes significant speedup vs calling the function from gf128mul.ko */
static inline void ocb_block_gfmul(union ocb_block *res,
				   const union ocb_block *x)
{
	u64 a = be64_to_cpu(x->b128.a);
	u64 b = be64_to_cpu(x->b128.b);
	u64 _tt = a & ((u64)1 << 63) ? 0x87 : 0;

	res->b128.a = cpu_to_be64((a << 1) | (b >> 63));
	res->b128.b = cpu_to_be64((b << 1) ^ _tt);
}

static inline void ocb_xor_padded(union ocb_block *dst, const u8 *src,
				  unsigned int size)
{
    crypto_xor(dst->bytes, src, size);
    dst->bytes[size] ^= 0x80;
}

static int crypto_ocb_setkey(struct crypto_aead *aead,
			     const u8 *key, unsigned int keylen)
{
	struct crypto_tfm *parent = crypto_aead_tfm(aead);
	struct ocb_ctx *ctx = crypto_aead_ctx(aead);
	union ocb_block zero_block;
	unsigned int i;
	int err;

	crypto_cipher_clear_flags(ctx->child, CRYPTO_TFM_REQ_MASK);
	crypto_cipher_set_flags(ctx->child, crypto_tfm_get_flags(parent) &
				CRYPTO_TFM_REQ_MASK);
	err = crypto_cipher_setkey(ctx->child, key, keylen);
	if (err)
		return err;

	crypto_tfm_set_flags(parent, crypto_cipher_get_flags(ctx->child) &
			     CRYPTO_TFM_RES_MASK);

	crypto_skcipher_clear_flags(ctx->ecb, CRYPTO_TFM_REQ_MASK);
	crypto_skcipher_set_flags(ctx->ecb, crypto_tfm_get_flags(parent) &
				  CRYPTO_TFM_REQ_MASK);
	err = crypto_skcipher_setkey(ctx->ecb, key, keylen);
	if (err)
		return err;

	crypto_tfm_set_flags(parent, crypto_skcipher_get_flags(ctx->ecb) &
			     CRYPTO_TFM_RES_MASK);

	ocb_block_set_zero(&zero_block);

	crypto_cipher_encrypt_one(ctx->child, ctx->l_star.bytes, zero_block.bytes);

	ocb_block_gfmul(&ctx->l_dollar, &ctx->l_star);
	ocb_block_gfmul(&ctx->l[0], &ctx->l_dollar);
	for (i = 1; i < ARRAY_SIZE(ctx->l); i++)
		ocb_block_gfmul(&ctx->l[i], &ctx->l[i - 1]);

	return 0;
}

static int crypto_ocb_setauthsize(struct crypto_aead *tfm,
				  unsigned int authsize)
{
	return (authsize <= OCB_BLOCK_SIZE) ? 0 : -EINVAL;
}

static int ocb_err_is_bad(struct aead_request *req, int err)
{
	switch (err) {
	case 0:
	case -EINPROGRESS:
		return 0;
	case -EBUSY:
		return !(aead_request_flags(req) & CRYPTO_TFM_REQ_MAY_BACKLOG);
	default:
		return 1;
	}
}

static void ocb_callback(struct crypto_async_request *subreq, int err)
{
	struct ocb_req_ctx *rctx = subreq->data;
	struct aead_request *req = rctx->parent;

	switch (err) {
	case 0:
		break;
	case -EINPROGRESS:
		return;
	default:
		aead_request_complete(req, err);
		return;
	}

	err = rctx->req_done(rctx, 0);
	if (err == 0 || ocb_err_is_bad(req, err))
		aead_request_complete(req, err);
}

/*
 * OCB crypto chain:
 * 1. PHASE #1 -- hash assoc. data, prepare payload
 * 4. ECB over payload
 * 5. PHASE #2 -- post-process payload, set/compare auth tag
 */

static int ocb_phase1(struct ocb_req_ctx *rctx, u32 flags);
static int ocb_phase2(struct ocb_req_ctx *rctx, u32 flags);

static void ocb_get_nonce_offset(struct ocb_req_ctx *rctx,
				 union ocb_block *offset, u8 *nonce,
				 unsigned int nonce_len)
{
	struct aead_request *req = rctx->parent;
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);
	unsigned int bottom = nonce[nonce_len - 1] & 0x3F;
	u64 a, b, c;

	ocb_block_set_zero(offset);
	offset->bytes[0] = (u8)((rctx->authsize * 8) << 1);
	offset->bytes[OCB_BLOCK_SIZE - nonce_len - 1] |= (u8)0x01;
	memcpy(&offset->bytes[OCB_BLOCK_SIZE - nonce_len], nonce, nonce_len);
	offset->bytes[OCB_BLOCK_SIZE - 1] &= (u8)0xC0;

	crypto_cipher_encrypt_one(ctx->child, offset->bytes, offset->bytes);

	a = be64_to_cpu(offset->b128.a);
	b = be64_to_cpu(offset->b128.b);
	c = a ^ ((a << 8) | (b >> 56));

	/* note that (x >> (63 - n) >> 1) is not the same as (x >> (64 - n)) */
	offset->b128.a = cpu_to_be64((a << bottom) | (b >> (63 - bottom) >> 1));
	offset->b128.b = cpu_to_be64((b << bottom) | (c >> (63 - bottom) >> 1));
}

static void ocb_ad_core_a(struct ocb_ctx *ctx, unsigned long *i,
			  union ocb_block *offset, union ocb_block *tag,
			  const void *in)
{
	union ocb_block tmp;
	unsigned long l_index = count_trailing_zeros(*i);
	ocb_block_xor(offset, offset, &ctx->l[l_index]);

	ocb_block_xor(&tmp, (const union ocb_block *)in, offset);
	crypto_cipher_encrypt_one(ctx->child, tmp.bytes, tmp.bytes);
	ocb_block_xor(tag, tag, &tmp);

	(*i)++;
}

static void ocb_ad_core_u(struct ocb_ctx *ctx, unsigned long *i,
			  union ocb_block *offset, union ocb_block *tag,
			  const void *in)
{
	union ocb_block tmp;
	unsigned long l_index = count_trailing_zeros(*i);
	ocb_block_xor(offset, offset, &ctx->l[l_index]);

	memcpy(tmp.bytes, in, OCB_BLOCK_SIZE);
	ocb_block_xor(&tmp, &tmp, offset);
	crypto_cipher_encrypt_one(ctx->child, tmp.bytes, tmp.bytes);
	ocb_block_xor(tag, tag, &tmp);

	(*i)++;
}

static void ocb_process_ad(struct ocb_req_ctx *rctx, struct scatterlist *sg_src,
			   unsigned int assoclen)
{
	struct aead_request *req = rctx->parent;
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);

	struct scatter_walk walk;
	unsigned long i;
	union ocb_block buf, offset;
	unsigned int pos = 0;

	ocb_block_set_zero(&offset);

	i = 1;
	scatterwalk_start(&walk, sg_src);
	while (assoclen != 0) {
		unsigned int size = scatterwalk_clamp(&walk, assoclen);
		unsigned int left = size;
		void *mapped = scatterwalk_map(&walk);
		const u8 *src = (const u8 *)mapped;

		if (pos + size >= OCB_BLOCK_SIZE) {
			if (pos > 0) {
				unsigned int fill = OCB_BLOCK_SIZE - pos;
				memcpy(buf.bytes + pos, src, fill);

				ocb_ad_core_a(ctx, &i, &offset, &rctx->tag,
					      buf.bytes);

				pos = 0;
				left -= fill;
				src += fill;
			}

			if (IS_ALIGNED((uintptr_t)src,
				       __alignof__(union ocb_block))) {
				while (left >= OCB_BLOCK_SIZE) {
					ocb_ad_core_a(ctx, &i, &offset,
						      &rctx->tag, src);

					left -= OCB_BLOCK_SIZE;
					src += OCB_BLOCK_SIZE;
				}
			} else {
				while (left >= OCB_BLOCK_SIZE) {
					ocb_ad_core_u(ctx, &i, &offset,
						      &rctx->tag, src);

					left -= OCB_BLOCK_SIZE;
					src += OCB_BLOCK_SIZE;
				}
			}
		}

		memcpy(buf.bytes + pos, src, left);

		pos += left;
		assoclen -= size;
		scatterwalk_unmap(mapped);
		scatterwalk_advance(&walk, size);
		scatterwalk_done(&walk, 0, assoclen);
	}

	if (pos > 0) {
		ocb_block_xor(&offset, &offset, &ctx->l_star);
		ocb_xor_padded(&offset, buf.bytes, pos);

		crypto_cipher_encrypt_one(ctx->child, offset.bytes,
					  offset.bytes);

		ocb_block_xor(&rctx->tag, &rctx->tag, &offset);
	}
}

static int ocb_phase1(struct ocb_req_ctx *rctx, u32 flags)
{
	struct aead_request *req = rctx->parent;
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);
	struct skcipher_request *subreq = &rctx->ecb_req;
	int err;

	struct skcipher_walk walk;
	unsigned int avail = 0;
	unsigned long i, l_index;
	const union ocb_block *cursor_in;
	union ocb_block *cursor_out;
	union ocb_block offset;

	ocb_process_ad(rctx, req->src, req->assoclen);

	ocb_get_nonce_offset(rctx, &offset, req->iv, OCB_NONCE_SIZE);
	rctx->iv_offset = offset;

	rctx->checksum = ctx->l_dollar;

	i = 1;
	if (rctx->decrypting)
		err = skcipher_walk_aead_decrypt(&walk, req, false);
	else
		err = skcipher_walk_aead_encrypt(&walk, req, false);
	if (err)
		return err;
	while (walk.nbytes) {
		avail       = walk.nbytes;
		cursor_in   = walk.src.virt.addr;
		cursor_out  = walk.dst.virt.addr;

		while (avail >= OCB_BLOCK_SIZE) {
			if (!rctx->decrypting)
				ocb_block_xor(&rctx->checksum, &rctx->checksum,
					      cursor_in);

			l_index = count_trailing_zeros(i);
			ocb_block_xor(&offset, &offset, &ctx->l[l_index]);
			ocb_block_xor(cursor_out, cursor_in, &offset);

			avail -= OCB_BLOCK_SIZE;
			++cursor_in;
			++cursor_out;
			++i;
		}

		if (unlikely(avail != 0))
			break;

		skcipher_walk_done(&walk, 0);
	}

	if (unlikely(avail != 0)) {
		ocb_block_xor(&offset, &offset, &ctx->l_star);
		ocb_block_xor(&rctx->checksum, &rctx->checksum, &offset);

		crypto_cipher_encrypt_one(ctx->child, offset.bytes,
					  offset.bytes);
		crypto_xor(offset.bytes, cursor_in->bytes, avail);

		if (rctx->decrypting)
			ocb_xor_padded(&rctx->checksum, offset.bytes, avail);
		else
			ocb_xor_padded(&rctx->checksum, cursor_in->bytes, avail);

		memcpy(cursor_out->bytes, offset.bytes, avail);
		skcipher_walk_done(&walk, 0);
	} else {
		ocb_block_xor(&rctx->checksum, &rctx->checksum, &offset);
	}

	rctx->req_done = ocb_phase2;
	skcipher_request_set_crypt(subreq, rctx->dst, rctx->dst,
				   rctx->cryptlen & OCB_BLOCK_MASK, NULL);
	skcipher_request_set_callback(subreq, flags, ocb_callback, rctx);
	err = rctx->crypt_fn(subreq);
	return err ?: ocb_phase2(rctx, flags);
}

static int ocb_phase2(struct ocb_req_ctx *rctx, u32 flags)
{
	struct aead_request *req = rctx->parent;
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);
	struct skcipher_request *subreq = &rctx->ecb_req;
	int err;

	struct skcipher_walk walk;
	unsigned int avail;
	unsigned long i, l_index;
	const union ocb_block *cursor_in;
	union ocb_block *cursor_out;
	union ocb_block offset;

	offset = rctx->iv_offset;

	i = 1;
	skcipher_request_set_crypt(subreq, rctx->dst, rctx->dst,
				   rctx->cryptlen & OCB_BLOCK_MASK, NULL);
	err = skcipher_walk_virt(&walk, subreq, false);
	if (err)
		return err;
	while (walk.nbytes) {
		avail       = walk.nbytes;
		cursor_in   = walk.src.virt.addr;
		cursor_out  = walk.dst.virt.addr;

		while (avail >= OCB_BLOCK_SIZE) {
			l_index = count_trailing_zeros(i);
			ocb_block_xor(&offset, &offset, &ctx->l[l_index]);
			ocb_block_xor(cursor_out, cursor_in, &offset);
			if (rctx->decrypting)
				ocb_block_xor(&rctx->checksum, &rctx->checksum,
					      cursor_out);

			avail -= OCB_BLOCK_SIZE;
			++cursor_in;
			++cursor_out;
			++i;
		}

		err = skcipher_walk_done(&walk, avail);
	}

	crypto_cipher_encrypt_one(ctx->child, rctx->checksum.bytes,
				  rctx->checksum.bytes);
	ocb_block_xor(&rctx->tag, &rctx->tag, &rctx->checksum);

	if (rctx->decrypting) {
		return ocb_block_is_zero(&rctx->tag) ? 0 : -EBADMSG;
	} else {
		scatterwalk_map_and_copy(rctx->tag.bytes, req->dst,
					 req->assoclen + rctx->cryptlen,
					 rctx->authsize, 1);
		return 0;
	}
}

static void ocb_init_common(struct ocb_req_ctx *rctx)
{
	struct aead_request *req = rctx->parent;
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);
	struct scatterlist *sg;

	ocb_block_set_zero(&rctx->tag);

	skcipher_request_set_tfm(&rctx->ecb_req, ctx->ecb);

	sg = scatterwalk_ffwd(rctx->sg_src, req->src, req->assoclen);
	if (sg == NULL) {
		sg_init_table(rctx->sg_src, 1);
		sg_set_page(rctx->sg_src, ZERO_PAGE(0), 0, 0);
	} else if (sg != rctx->sg_src)
		*rctx->sg_src = *sg;

	rctx->dst = rctx->src = rctx->sg_src;
	if (req->src != req->dst) {
		rctx->dst = rctx->sg_dst;

		sg = scatterwalk_ffwd(rctx->sg_dst, req->dst, req->assoclen);
		if (sg == NULL) {
			sg_init_table(rctx->sg_dst, 1);
			sg_set_page(rctx->sg_dst, ZERO_PAGE(0), 0, 0);
		} else if (sg != rctx->sg_dst)
			*rctx->sg_dst = *sg;
	}
}

static int crypto_ocb_encrypt(struct aead_request *req)
{
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	u32 flags = aead_request_flags(req);
	unsigned long align = crypto_aead_alignmask(tfm);
	struct ocb_req_ctx *rctx =
		(void *)PTR_ALIGN((u8 *)aead_request_ctx(req), align + 1);

	rctx->parent = req;
	rctx->authsize = crypto_aead_authsize(tfm);
	rctx->cryptlen = req->cryptlen;
	rctx->crypt_fn = crypto_skcipher_encrypt;
	rctx->decrypting = 0;

	ocb_init_common(rctx);

	return ocb_phase1(rctx, flags);
}

static int crypto_ocb_decrypt(struct aead_request *req)
{
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	u32 flags = aead_request_flags(req);
	unsigned long align = crypto_aead_alignmask(tfm);
	struct ocb_req_ctx *rctx =
		(void *)PTR_ALIGN((u8 *)aead_request_ctx(req), align + 1);

	rctx->parent = req;
	rctx->authsize = crypto_aead_authsize(tfm);
	rctx->cryptlen = req->cryptlen - rctx->authsize;
	rctx->crypt_fn = crypto_skcipher_decrypt;
	rctx->decrypting = 1;

	ocb_init_common(rctx);

	scatterwalk_map_and_copy(rctx->tag.bytes, req->src,
				 req->assoclen + rctx->cryptlen,
				 rctx->authsize, 0);

	return ocb_phase1(rctx, flags);
}

static int crypto_ocb_init_tfm(struct crypto_aead *tfm)
{
	struct aead_instance *inst = aead_alg_instance(tfm);
	struct ocb_instance_ctx *inst_ctx = aead_instance_ctx(inst);
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);
	unsigned int ecb_reqsize;

	ctx->child = crypto_spawn_cipher(&inst_ctx->spawn);
	if (IS_ERR(ctx->child)) {
		return PTR_ERR(ctx->child);
	}

	ctx->ecb = crypto_spawn_skcipher(&inst_ctx->ecb_spawn);
	if (IS_ERR(ctx->ecb)) {
		crypto_free_cipher(ctx->child);
		return PTR_ERR(ctx->ecb);
	}

	ecb_reqsize = crypto_skcipher_reqsize(ctx->ecb);

	tfm->reqsize = sizeof(struct ocb_req_ctx) + ecb_reqsize;
	return 0;
}

static void crypto_ocb_exit_tfm(struct crypto_aead *tfm)
{
	struct ocb_ctx *ctx = crypto_aead_ctx(tfm);

	crypto_free_skcipher(ctx->ecb);
}

static void crypto_ocb_free(struct aead_instance *inst)
{
	struct ocb_instance_ctx *ctx = aead_instance_ctx(inst);

	crypto_drop_skcipher(&ctx->ecb_spawn);
	kfree(inst);
}

static int crypto_ocb_create(struct crypto_template *tmpl,
			     struct rtattr **tb)
{
	struct aead_instance *inst;
	struct ocb_instance_ctx *ctx;
	struct crypto_alg *alg;
	struct skcipher_alg *ecb_alg;
	char ecb_name[CRYPTO_MAX_ALG_NAME];
	int err;

	err = crypto_check_attr_type(tb, CRYPTO_ALG_TYPE_AEAD);
	if (err)
		return err;

	alg = crypto_get_attr_alg(tb, CRYPTO_ALG_TYPE_CIPHER,
				  CRYPTO_ALG_TYPE_MASK);
	if (IS_ERR(alg))
		return PTR_ERR(alg);

	/* we only support 16-byte blocks: */
	if (alg->cra_blocksize != OCB_BLOCK_SIZE)
		return -EINVAL;

	inst = kzalloc(sizeof(*inst) + sizeof(*ctx), GFP_KERNEL);
	if (!inst) {
		err = -ENOMEM;
		goto out_put_alg;
	}
	ctx = aead_instance_ctx(inst);

	/* prepare spawn for child cipher: */
	err = crypto_init_spawn(&ctx->spawn, alg, aead_crypto_instance(inst),
				CRYPTO_ALG_TYPE_MASK | CRYPTO_ALG_ASYNC);
	if (err)
		goto err_free_inst;

	/* prepare spawn for ECB mode: */
	err = -ENAMETOOLONG;
	if (snprintf(ecb_name, CRYPTO_MAX_ALG_NAME, "ecb(%s)", alg->cra_name)
			>= CRYPTO_MAX_ALG_NAME)
		goto err_drop_spawn;

	crypto_set_skcipher_spawn(&ctx->ecb_spawn, aead_crypto_instance(inst));
	err = crypto_grab_skcipher(&ctx->ecb_spawn, ecb_name, 0, 0);
	if (err)
		goto err_drop_spawn;

	/* get the crypto_alg for the ECB mode: */
	ecb_alg = crypto_skcipher_spawn_alg(&ctx->ecb_spawn);

	err = -ENAMETOOLONG;
	if (snprintf(inst->alg.base.cra_name, CRYPTO_MAX_ALG_NAME, "ocb(%s)",
				 alg->cra_name) >= CRYPTO_MAX_ALG_NAME)
		goto err_drop_ecb_spawn;

	if (snprintf(inst->alg.base.cra_driver_name, CRYPTO_MAX_ALG_NAME,
		     "ocb(%s,%s)",
		     alg->cra_driver_name, ecb_alg->base.cra_driver_name)
			>= CRYPTO_MAX_ALG_NAME)
		goto err_drop_ecb_spawn;

	inst->alg.base.cra_flags = CRYPTO_ALG_TYPE_AEAD |
			(ecb_alg->base.cra_flags & CRYPTO_ALG_ASYNC);
	inst->alg.base.cra_priority = ecb_alg->base.cra_priority * 50 +
			alg->cra_priority;
	inst->alg.base.cra_blocksize = 1;
	inst->alg.base.cra_alignmask = ecb_alg->base.cra_alignmask;

	inst->alg.chunksize = OCB_BLOCK_SIZE;
	inst->alg.ivsize = OCB_NONCE_SIZE;
	inst->alg.maxauthsize = OCB_BLOCK_SIZE;

	inst->alg.setkey = crypto_ocb_setkey;
	inst->alg.setauthsize = crypto_ocb_setauthsize;
	inst->alg.encrypt = crypto_ocb_encrypt;
	inst->alg.decrypt = crypto_ocb_decrypt;

	inst->alg.base.cra_ctxsize = sizeof(struct ocb_ctx);

	inst->alg.init = crypto_ocb_init_tfm;
	inst->alg.exit = crypto_ocb_exit_tfm;

	inst->free = crypto_ocb_free;

	err = aead_register_instance(tmpl, inst);
	if (err)
		goto err_drop_ecb_spawn;

	goto out_put_alg;

err_drop_ecb_spawn:
	crypto_drop_skcipher(&ctx->ecb_spawn);
err_drop_spawn:
	crypto_drop_spawn(&ctx->spawn);
err_free_inst:
	kzfree(inst);
out_put_alg:
	crypto_mod_put(alg);
	return err;
}

static struct crypto_template crypto_ocb_tmpl = {
	.name = "ocb",
	.create = crypto_ocb_create,
	.module = THIS_MODULE,
};


static int __init crypto_ocb_module_init(void)
{
	return crypto_register_template(&crypto_ocb_tmpl);
}

static void __exit crypto_ocb_module_exit(void)
{
	crypto_unregister_template(&crypto_ocb_tmpl);
}

module_init(crypto_ocb_module_init);
module_exit(crypto_ocb_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ondrej Mosnacek <omosnacek@gmail.com>");
MODULE_DESCRIPTION("OCB AEAD mode");
MODULE_ALIAS_CRYPTO("ocb");
