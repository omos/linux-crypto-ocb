/*
 * The AES-OCB Authenticated-Encryption Algorithm
 *   Glue for AES-NI + SSE2 implementation
 *
 * Copyright (c) 2016 Ondrej Mosnacek <omosnacek@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */

#include <crypto/cryptd.h>
#include <crypto/scatterwalk.h>
#include <crypto/internal/aead.h>
#include <crypto/internal/skcipher.h>
#include <linux/module.h>
#include <asm/fpu/api.h>
#include <asm/cpu_device_id.h>

#define OCB_BLOCK_SIZE 16
#define OCB_NONCE_SIZE 12 /* FIXME */
#define OCB_MAX_AUTH_SIZE OCB_BLOCK_SIZE
#define AES_KEY_SCHED_SIZE (480 + 16)
#define L_TABLE_ENTRIES 32

asmlinkage int aesni_set_key(void *ctx, const u8 *in_key, unsigned int key_len);

asmlinkage void crypto_ocb_aes_aesni_init(void *ctx);

asmlinkage void crypto_ocb_aes_aesni_ad(
		const void *ctx, void *counter, void *offset, void *tag_xor,
		unsigned int length, const void *data);

asmlinkage void crypto_ocb_aes_aesni_ad_tail(
		const void *ctx, const void *offset, void *tag_xor,
		unsigned int length, const void *data);

asmlinkage void crypto_ocb_aes_aesni_start(
		const void *ctx, void *offset, const void *nonce);

asmlinkage void crypto_ocb_aes_aesni_enc(
		const void *ctx, void *counter, void *offset, void *checksum,
		unsigned int length, const void *src, void *dst);

asmlinkage void crypto_ocb_aes_aesni_dec(
		const void *ctx, void *counter, void *offset, void *checksum,
		unsigned int length, const void *src, void *dst);

asmlinkage void crypto_ocb_aes_aesni_enc_tail(
		const void *ctx, void *offset, void *checksum,
		unsigned int length, const void *src, void *dst);

asmlinkage void crypto_ocb_aes_aesni_dec_tail(
		const void *ctx, void *offset, void *checksum,
		unsigned int length, const void *src, void *dst);

asmlinkage void crypto_ocb_aes_aesni_final(
		const void *ctx, const void *offset, const void *checksum,
		void *tag_xor);

struct ocb_block {
	u8 bytes[OCB_BLOCK_SIZE] __aligned(16);
};

struct aes_key_sched {
	u8 bytes[AES_KEY_SCHED_SIZE] __aligned(16);
};

struct ocb_aes_ctx {
	struct aes_key_sched key_sched;
	struct ocb_block l_star;
	struct ocb_block l_dollar;
	struct ocb_block l_table[L_TABLE_ENTRIES];
};

struct ocb_crypt_ops {
	int (*skcipher_walk_init)(struct skcipher_walk *walk,
				  struct aead_request *req, bool atomic);

	void (*crypt_blocks)(const void *ctx, void *counter, void *offset,
			     void *checksum, unsigned int length,
			     const void *src, void *dst);
	void (*crypt_tail)(const void *ctx, void *offset, void *checksum,
			   unsigned int length, const void *src, void *dst);
};

static void crypto_ocb_aes_aesni_process_ad(
		const struct ocb_aes_ctx *ctx, struct ocb_block *tag_xor,
		struct scatterlist *sg_src, unsigned int assoclen)
{
	struct scatter_walk walk;
	struct ocb_block buf, offset = {};
	unsigned int pos = 0;
	u64 counter = 0;

	scatterwalk_start(&walk, sg_src);
	while (assoclen != 0) {
		unsigned int size = scatterwalk_clamp(&walk, assoclen);
		unsigned int left = size;
		void *mapped = scatterwalk_map(&walk);
		const u8 *src = (const u8 *)mapped;

		if (pos + size >= OCB_BLOCK_SIZE) {
			if (pos > 0) {
				unsigned int fill = OCB_BLOCK_SIZE - pos;
				memcpy(buf.bytes + pos, src, fill);
				crypto_ocb_aes_aesni_ad(ctx, &counter, &offset,
							tag_xor, OCB_BLOCK_SIZE,
							buf.bytes);
				pos = 0;
				left -= fill;
				src += fill;
			}

			crypto_ocb_aes_aesni_ad(ctx, &counter, &offset, tag_xor,
						left, src);

			src += left & ~(OCB_BLOCK_SIZE - 1);
			left &= OCB_BLOCK_SIZE - 1;
		}

		memcpy(buf.bytes + pos, src, left);

		pos += left;
		assoclen -= size;
		scatterwalk_unmap(mapped);
		scatterwalk_advance(&walk, size);
		scatterwalk_done(&walk, 0, assoclen);
	}

	if (pos > 0) {
		crypto_ocb_aes_aesni_ad_tail(ctx, &offset, tag_xor,
					     pos, buf.bytes);
	}
}

static void crypto_ocb_aes_aesni_process_crypt(
		const struct ocb_aes_ctx *ctx, const struct ocb_crypt_ops *ops,
		const u8 *iv, struct ocb_block *tag_xor, unsigned int authsize,
		struct aead_request *req)
{
	struct skcipher_walk walk;
	u8 *cursor_src, *cursor_dst;
	unsigned int chunksize, base;
	struct ocb_block buf, offset, checksum = {};
	u64 counter = 0;

	memset(buf.bytes, 0 , OCB_BLOCK_SIZE);
	buf.bytes[0] = (u8)((authsize * 8) << 1);
	buf.bytes[OCB_BLOCK_SIZE - OCB_NONCE_SIZE - 1] |= (u8)0x01;
	memcpy(&buf.bytes[OCB_BLOCK_SIZE - OCB_NONCE_SIZE], iv, OCB_NONCE_SIZE);

	crypto_ocb_aes_aesni_start(ctx, &offset, &buf);

	ops->skcipher_walk_init(&walk, req, false);
	while (walk.nbytes) {
		cursor_src = walk.src.virt.addr;
		cursor_dst = walk.dst.virt.addr;
		chunksize = walk.nbytes;

		ops->crypt_blocks(ctx, &counter, &offset, &checksum, chunksize,
				  cursor_src, cursor_dst);

		base = chunksize & ~(OCB_BLOCK_SIZE - 1);
		cursor_src += base;
		cursor_dst += base;
		chunksize &= OCB_BLOCK_SIZE - 1;

		if (chunksize > 0) {
			ops->crypt_tail(ctx, &offset, &checksum, chunksize,
					cursor_src, cursor_dst);
		}

		skcipher_walk_done(&walk, 0);
	}

	crypto_ocb_aes_aesni_final(ctx, &offset, &checksum, tag_xor);
}

static struct ocb_aes_ctx *crypto_ocb_aes_aesni_ctx(struct crypto_aead *aead)
{
	u8 *ctx = crypto_aead_ctx(aead);
	ctx = PTR_ALIGN(ctx, __alignof__(struct ocb_aes_ctx));
	return (void *)ctx;
}

static int crypto_ocb_aes_aesni_setkey(struct crypto_aead *aead, const u8 *key,
				       unsigned int keylen)
{
	struct ocb_aes_ctx *ctx = crypto_ocb_aes_aesni_ctx(aead);

	int err = aesni_set_key(&ctx->key_sched, key, keylen);
	if (err) {
		crypto_aead_set_flags(aead, CRYPTO_TFM_RES_BAD_KEY_LEN);
		return err;
	}

	crypto_ocb_aes_aesni_init(ctx);

	return 0;
}

static int crypto_ocb_aes_aesni_setauthsize(struct crypto_aead *tfm,
					    unsigned int authsize)
{
	return (authsize <= OCB_MAX_AUTH_SIZE) ? 0 : -EINVAL;
}

static void crypto_ocb_aes_aesni_crypt(struct aead_request *req,
				       const struct ocb_crypt_ops *ops,
				       struct ocb_block *tag_xor)
{
	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_aes_ctx *ctx = crypto_ocb_aes_aesni_ctx(tfm);

	kernel_fpu_begin();

	crypto_ocb_aes_aesni_process_ad(ctx, tag_xor, req->src, req->assoclen);
	crypto_ocb_aes_aesni_process_crypt(ctx, ops, req->iv, tag_xor,
					   crypto_aead_authsize(tfm), req);

	kernel_fpu_end();
}

static int crypto_ocb_aes_aesni_encrypt(struct aead_request *req)
{
	static const struct ocb_crypt_ops OPS = {
		.skcipher_walk_init = skcipher_walk_aead_encrypt,
		.crypt_blocks = crypto_ocb_aes_aesni_enc,
		.crypt_tail = crypto_ocb_aes_aesni_enc_tail,
	};

	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_block tag = {};
	unsigned int authsize = crypto_aead_authsize(tfm);
	unsigned int cryptlen = req->cryptlen;

	crypto_ocb_aes_aesni_crypt(req, &OPS, &tag);

	scatterwalk_map_and_copy(tag.bytes, req->dst,
				 req->assoclen + cryptlen, authsize, 1);
	return 0;
}

static int crypto_ocb_aes_aesni_decrypt(struct aead_request *req)
{
	static const struct ocb_block zeros = {};

	static const struct ocb_crypt_ops OPS = {
		.skcipher_walk_init = skcipher_walk_aead_decrypt,
		.crypt_blocks = crypto_ocb_aes_aesni_dec,
		.crypt_tail = crypto_ocb_aes_aesni_dec_tail,
	};

	struct crypto_aead *tfm = crypto_aead_reqtfm(req);
	struct ocb_block tag;
	unsigned int authsize = crypto_aead_authsize(tfm);
	unsigned int cryptlen = req->cryptlen - authsize;

	scatterwalk_map_and_copy(tag.bytes, req->src,
				 req->assoclen + cryptlen, authsize, 0);

	crypto_ocb_aes_aesni_crypt(req, &OPS, &tag);

	return crypto_memneq(tag.bytes, zeros.bytes, authsize) ? -EBADMSG : 0;
}

static int crypto_ocb_aes_aesni_init_tfm(struct crypto_aead *aead)
{
	return 0;
}

static void crypto_ocb_aes_aesni_exit_tfm(struct crypto_aead *aead)
{
}

static int cryptd_ocb_aes_aesni_setkey(struct crypto_aead *aead,
				       const u8 *key, unsigned int keylen)
{
	struct cryptd_aead **ctx = crypto_aead_ctx(aead);
	struct cryptd_aead *cryptd_tfm = *ctx;

	return crypto_aead_setkey(&cryptd_tfm->base, key, keylen);
}

static int cryptd_ocb_aes_aesni_setauthsize(struct crypto_aead *aead,
					    unsigned int authsize)
{
	struct cryptd_aead **ctx = crypto_aead_ctx(aead);
	struct cryptd_aead *cryptd_tfm = *ctx;

	return crypto_aead_setauthsize(&cryptd_tfm->base, authsize);
}

static int cryptd_ocb_aes_aesni_encrypt(struct aead_request *req)
{
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	struct cryptd_aead **ctx = crypto_aead_ctx(aead);
	struct cryptd_aead *cryptd_tfm = *ctx;

	aead = &cryptd_tfm->base;
	if (irq_fpu_usable() && (!in_atomic() ||
				 !cryptd_aead_queued(cryptd_tfm)))
		aead = cryptd_aead_child(cryptd_tfm);

	aead_request_set_tfm(req, aead);

	return crypto_aead_encrypt(req);
}

static int cryptd_ocb_aes_aesni_decrypt(struct aead_request *req)
{
	struct crypto_aead *aead = crypto_aead_reqtfm(req);
	struct cryptd_aead **ctx = crypto_aead_ctx(aead);
	struct cryptd_aead *cryptd_tfm = *ctx;

	aead = &cryptd_tfm->base;
	if (irq_fpu_usable() && (!in_atomic() ||
				 !cryptd_aead_queued(cryptd_tfm)))
		aead = cryptd_aead_child(cryptd_tfm);

	aead_request_set_tfm(req, aead);

	return crypto_aead_decrypt(req);
}

static int cryptd_ocb_aes_aesni_init_tfm(struct crypto_aead *aead)
{
	struct cryptd_aead *cryptd_tfm;
	struct cryptd_aead **ctx = crypto_aead_ctx(aead);

	cryptd_tfm = cryptd_alloc_aead("__ocb-aes-aesni", CRYPTO_ALG_INTERNAL,
				       CRYPTO_ALG_INTERNAL);
	if (IS_ERR(cryptd_tfm))
		return PTR_ERR(cryptd_tfm);

	*ctx = cryptd_tfm;
	crypto_aead_set_reqsize(aead, crypto_aead_reqsize(&cryptd_tfm->base));
	return 0;
}

static void cryptd_ocb_aes_aesni_exit_tfm(struct crypto_aead *aead)
{
	struct cryptd_aead **ctx = crypto_aead_ctx(aead);

	cryptd_free_aead(*ctx);
}

static struct aead_alg crypto_ocb_aes_aesni_algs[2] = {
	{
		.setkey = crypto_ocb_aes_aesni_setkey,
		.setauthsize = crypto_ocb_aes_aesni_setauthsize,
		.encrypt = crypto_ocb_aes_aesni_encrypt,
		.decrypt = crypto_ocb_aes_aesni_decrypt,
		.init = crypto_ocb_aes_aesni_init_tfm,
		.exit = crypto_ocb_aes_aesni_exit_tfm,

		.ivsize = OCB_NONCE_SIZE,
		.maxauthsize = OCB_MAX_AUTH_SIZE,
		.chunksize = OCB_BLOCK_SIZE,

		.base = {
			.cra_flags = CRYPTO_ALG_INTERNAL,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct ocb_aes_ctx),
			.cra_alignmask = 0xF,

			.cra_name = "__ocb(aes)",
			.cra_driver_name = "__ocb-aes-aesni",

			.cra_module = THIS_MODULE,
		}
	}, {
		.setkey = cryptd_ocb_aes_aesni_setkey,
		.setauthsize = cryptd_ocb_aes_aesni_setauthsize,
		.encrypt = cryptd_ocb_aes_aesni_encrypt,
		.decrypt = cryptd_ocb_aes_aesni_decrypt,
		.init = cryptd_ocb_aes_aesni_init_tfm,
		.exit = cryptd_ocb_aes_aesni_exit_tfm,

		.ivsize = OCB_NONCE_SIZE,
		.maxauthsize = OCB_MAX_AUTH_SIZE,
		.chunksize = OCB_BLOCK_SIZE,

		.base = {
			.cra_flags = CRYPTO_ALG_ASYNC,
			.cra_blocksize = 1,
			.cra_ctxsize = sizeof(struct cryptd_aead *),
			.cra_alignmask = 0xF,

			.cra_priority = 400,

			.cra_name = "ocb(aes)",
			.cra_driver_name = "ocb-aes-aesni",

			.cra_module = THIS_MODULE,
		}
	}
};

static const struct x86_cpu_id aesni_cpu_id[] = {
    X86_FEATURE_MATCH(X86_FEATURE_XMM2),
    X86_FEATURE_MATCH(X86_FEATURE_AES),
    {}
};
MODULE_DEVICE_TABLE(x86cpu, aesni_cpu_id);

static int __init crypto_ocb_aes_aesni_module_init(void)
{
	if (!x86_match_cpu(aesni_cpu_id))
		return -ENODEV;

	return crypto_register_aeads(crypto_ocb_aes_aesni_algs,
				     ARRAY_SIZE(crypto_ocb_aes_aesni_algs));
}

static void __exit crypto_ocb_aes_aesni_module_exit(void)
{
	crypto_unregister_aeads(crypto_ocb_aes_aesni_algs,
				ARRAY_SIZE(crypto_ocb_aes_aesni_algs));
}

module_init(crypto_ocb_aes_aesni_module_init);
module_exit(crypto_ocb_aes_aesni_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ondrej Mosnacek <omosnacek@gmail.com>");
MODULE_DESCRIPTION("AES-OCB AEAD mode -- AESNI+SSE2 implementation");
MODULE_ALIAS_CRYPTO("ocb(aes)");
