TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib

DEFINES += __KERNEL__

ARCH=x86
SRC_PROJECT_PATH = $$PWD
LINUX_VERSION = $$system(uname -r)
LINUX_HEADERS_PATH = /lib/modules/$$LINUX_VERSION/build

INCLUDEPATH += $$LINUX_HEADERS_PATH/include
INCLUDEPATH += $$LINUX_HEADERS_PATH/arch/$$ARCH/include

buildmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH modules
cleanmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH clean
QMAKE_EXTRA_TARGETS += buildmod cleanmod

HEADERS += \
    ocb_tv.h

SOURCES += \
    ocb.c \
    ocb_test.c \
    ocb-aes-aesni-glue.c \
    ocb-aes-aesni-asm.S

DISTFILES += \
    Makefile
