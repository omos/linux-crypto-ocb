obj-m += ocb.o ocb_test.o

ifeq ($(CONFIG_X86_64),y)
	obj-m += ocb-aesni.o
endif

ocb-aesni-objs += ocb-aes-aesni-asm.o ocb-aes-aesni-glue.o

ifeq ($(KERNEL_BUILD),)
	ifeq ($(KERNEL_VERSION),)
		KERNEL_VERSION=$(shell uname -r)
	endif
	KERNEL_BUILD=/lib/modules/$(KERNEL_VERSION)/build
endif

all:
	make -C $(KERNEL_BUILD) M=$(PWD) modules

clean:
	make -C $(KERNEL_BUILD) M=$(PWD) clean
