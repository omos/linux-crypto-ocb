# A Linux kernel module implementing the OCB authenticated encryption mode

This project aims to implement the OCB AEAD mode as specified in [RFC 7253](https://tools.ietf.org/html/rfc7253) as a crypto module for the Linux kernel.

## Building

To build the OCB modules, you need to have the header files for the Linux kernel installed (`sudo apt-get install linux-headers-generic` on Ubuntu). Then, just run `make`.

## Installing

**WARNING**: The modules are still in development and may crash or break your machine! It is highly recommended that you only use them inside a virtual machine.

To install the modules, run `./load-all.sh` (as root). To remove the modules, run `./unload.sh`.

To see if the tests passed, run `dmesg | less +G`.
